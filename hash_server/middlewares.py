from aiohttp import web

from hash_server.errors import BaseApiError


def json_error(code, message):
    return web.json_response(data={'code': code, 'error': message})


async def error_middleware(app, handler):
    async def middleware_handler(request):
        try:
            response = await handler(request)
            if response.status == 404:
                return json_error(response.status, response.message)
            return response

        except BaseApiError as e:
            return json_error(e.code, e.msg)

        except web.HTTPException as ex:
            if ex.status == 404:
                return json_error(ex.status, ex.reason)
            raise
    return middleware_handler
