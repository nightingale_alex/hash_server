import contextlib
import os
from pathlib import PurePath

try:
    from dotenv import load_dotenv
except ImportError:
    load_dotenv = None

__all__ = ['Settings', 'settings']

_PACKAGE_DIR = PurePath(__file__).parent
_PROJECT_DIR = _PACKAGE_DIR.parent


class Settings:
    """Main settings."""

    def __init__(self):
        if load_dotenv:
            load_dotenv(dotenv_path=os.path.join(_PROJECT_DIR, '.env'), override=False)

        self.PROJECT_DIR = _PROJECT_DIR
        self.PACKAGE_DIR = _PACKAGE_DIR

        self.HTTP_PORT = int(os.getenv('HTTP_PORT', 80))

    @contextlib.contextmanager
    def temp_set_attributes(self, **kwargs):
        old_values = {}
        for attr, new_value in kwargs.items():
            if hasattr(self, attr):
                old_values[attr] = getattr(self, attr)
                setattr(self, attr, new_value)
            else:
                raise AttributeError(f'{self.__class__} does`t have such attribute')
        yield
        for attr, old_value in old_values.items():
            setattr(self, attr, old_value)


settings = Settings()
