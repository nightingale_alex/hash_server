from hash_server.views import get_hash_from_string, healthcheck


def setup_routes(app):
    app.router.add_get(r'/api/healthcheck', healthcheck, name='healthcheck')
    app.router.add_post(r'/api/hash/string', get_hash_from_string, name='get_hash_from_string')
