from aiohttp import web
import logging

from hash_server.errors import ValidationError

logger = logging.getLogger(__name__)


async def get_body_from_request_or_400(request: web.Request):
    """ Get body from web Request or raise Validation error """
    try:
        body = await request.json()
    except Exception as e:
        logger.warning(f'Exception on parsing request: {e!r}')
        raise ValidationError(msg='Request must be JSON')
    if not isinstance(body, dict):
        logger.warning(f'Invalid request: {body}')
        raise ValidationError(msg='Request must be a JSON object')
    return body
