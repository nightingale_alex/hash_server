import logging

from aiohttp import web

from hash_server.config import settings
from hash_server.middlewares import error_middleware
from hash_server.routes import setup_routes

logger = logging.getLogger(__name__)

__all__ = ['Server']


class Server:

    def __init__(self):
        self._app_class = web.Application
        self._app = None
        self._initialized = False

    def setup(self):
        """Initialises Server and all dependencies."""
        assert not self._initialized, 'Tried to initialize already initialized Server'
        self._initialized = True

        self._app = self._app_class(middlewares=[error_middleware])
        self.setup_http_endpoints()

        self._app.server = self

    @property
    def aiohttp_app(self) -> web.Application:
        """Underlying aiohttp.web.Application used for HTTP and async startup/shutdown."""
        return self._app

    def setup_http_endpoints(self):
        """Configures HTTP endpoints. """
        setup_routes(self._app)

    def run(self, http_port=None):
        """Run aiohttp server."""
        if not self._initialized:
            self.setup()

        if http_port:
            settings.HTTP_PORT = http_port

        web.run_app(self._app, port=settings.HTTP_PORT)
