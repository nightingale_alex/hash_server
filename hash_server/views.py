from aiohttp import web

from hash_server.errors import ValidationError, CriticalApiError
from hash_server.hash_class import HashClass
from hash_server.views_helpers import get_body_from_request_or_400


async def healthcheck(request):
    """
    Method representing a service health check
    :return: HTTP 200
    """
    return web.json_response(data=dict())


async def get_hash_from_string(request):
    """
    Method returns hash from string
    :return: 18 digit int hash from string
    """
    body = await get_body_from_request_or_400(request)
    hash_string = body.get('hash_string')
    salt = body.get('salt', '')

    hash_object = HashClass()

    try:
        hash_object.calculate_hash(string=hash_string, salt=salt)
    except ValueError as e:
        raise ValidationError(msg=f'{e!r}')
    except Exception as e:
        raise CriticalApiError(msg=f'{e!r}')

    return web.json_response(data={'hash': hash_object.get_hash_value_integer(max_digits_number=18)})

