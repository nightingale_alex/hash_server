import hashlib


class HashClass:
    def __init__(self, hash_func=hashlib.sha256, *args, **kwargs):
        self._hash_func = hash_func
        self.hash_value = None

    def calculate_hash(self, string: str, salt: str = ''):
        if not isinstance(string, str) or not isinstance(salt, str):
            raise ValueError

        string_to_hash_bytes = str.encode(string + salt)
        self.hash_value = self._hash_func(string_to_hash_bytes)

    def get_hash_value_digest(self):
        return self.hash_value.digest()

    def get_hash_value_hexdigest(self):
        return self.hash_value.hexdigest()

    def get_hash_value_integer(self, max_digits_number: int = None):
        result = int(self.get_hash_value_hexdigest(), 16)

        if max_digits_number:
            result = result % 10 ** max_digits_number

        return result
