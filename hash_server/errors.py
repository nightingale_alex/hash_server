class BaseApiError(Exception):
    def __init__(self, code=None, msg=None):
        self.code = self.code or code
        self.msg = self.msg or msg


class ValidationError(BaseApiError):
    code = 400
    msg = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class CriticalApiError(BaseApiError):
    code = 500
    msg = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


