import pytest

from hash_server.app import Server


@pytest.fixture
async def server(loop):
    server = Server()
    server.setup()
    return server


@pytest.fixture
async def app(server):
    return server.aiohttp_app


@pytest.fixture
async def client(aiohttp_client, app):
    return await aiohttp_client(app)
