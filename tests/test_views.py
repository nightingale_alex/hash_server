async def test_healthcheck(client):
    resp = await client.get('/api/healthcheck')
    assert resp.status == 200
    assert await resp.json() == {}


class TestHashEndpoint:
    async def test_positive(self, client):
        resp = await client.post('api/hash/string', json={'hash_string': 'a', 'salt': 'aa'})

        assert resp.status == 200

        json_resp = await resp.json()
        assert json_resp['hash'] == 810195356539873520

        # check for repeat same

        resp = await client.post('api/hash/string', json={'hash_string': 'a', 'salt': 'aa'})

        assert resp.status == 200

        json_resp = await resp.json()
        assert json_resp['hash'] == 810195356539873520

        # test diff without salt
        resp = await client.post('api/hash/string', json={'hash_string': 'a'})

        assert resp.status == 200

        json_resp = await resp.json()
        assert json_resp['hash'] == 601050179556493499
